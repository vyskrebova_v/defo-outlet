<?php error_reporting(E_ALL);?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="//<?= $_SERVER['HTTP_HOST']?>/css/style.css">
	<title>Дэфо - Outlet</title>
</head>
<body>
<div class="header">
	<div class="top-banner"></div>
	<div class="header__mobile d-lg-none">
		<div class="header__logo col-12 row align-self-start">
			<div class="logo__image col col-auto align-self-start">
				<a href="//<?= $_SERVER['HTTP_HOST']?>" class="logo__img"></a>
			</div>
			<div class="logo__text col align-self-center">
				<span class="logo_text__span">Outlet <span class="logo_text_span__emphasized">Defo</span></span>
			</div>
		</div>
	</div>
	<div class="header__container d-flex flex-column d-lg-block">
		<div class="header__additional_bar d-lg-none order-1">
			<div class="header__logo col-12 row align-self-center">
				<div class="logo__text col align-self-center">
					<span class="logo_text__span">Outlet <span class="logo_text_span__emphasized">Defo</span></span>
				</div>
			</div>
		</div>
		<div class="header__info order-3">
			<div class="header_info__container container">
				<div class="header_info__row row">
					<div class="header__logo col-sm-12 col-lg-4 col-4 row align-self-start d-none d-lg-flex">
						<div class="logo__image col col-sm-auto align-self-start">
							<a href="//<?= $_SERVER['HTTP_HOST']?>" class="logo__img"></a>
						</div>
						<div class="logo__text col align-self-center">
							<span class="logo_text__span">Outlet <span class="logo_text_span__emphasized">Defo</span></span>
						</div>
					</div>
					<div class="header__descriptor col-3 align-self-center d-none d-lg-inline">
						<span class="description__span row justify-content-start">Лучшие цены</span>
						<span class="description__span row justify-content-end">на 10 000 моделей</span>
					</div>
					<div class="header__contacts col-lg row align-self-center">
						<span class="header_contacts__place col-12">Санкт-Петербург, МЦ Аквилон</span>
						<span class="header_contacts__phone col-12">
									<a href="tel:8 (812) 596-34-76" class="contacts_phone__link">8 (812) 596-34-76</a>
								</span>
						<span class="header_contacts__address col-12">Новолитовская ул., 15 Д</span>
					</div>
					<div class="header__contacts col-lg row align-self-center">
						<span class="header_contacts__place col-12">Москва, ТЦ Горбушкин двор</span>
						<span class="header_contacts__phone col-12">
									<a href="tel:8 (812) 596-34-76" class="contacts_phone__link">8 (812) 596-34-76</a>
								</span>
						<span class="header_contacts__address col-12">Багратионовский пр., 7</span>
					</div>
				</div>
			</div>
		</div>
		<div class="header__menu order-2">
			<div class="header_menu__container container">
				<ul class="header_menu__list row justify-content-between">
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#stoly" class="header_menu_link">Обеденные столы</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#stulya-dlya-doma" class="header_menu_link">Стулья для дома</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#komputernye-kresla" class="header_menu_link">Компьютерные кресла</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#detskie" class="header_menu_link">Детские</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#gostinnye" class="header_menu_link">Гостиные</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#spalni" class="header_menu_link">Спальни</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#komputernye-kresla" class="header_menu_link">Офисная мебель</a>
					</li>
					<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">
						<a href="//<?= $_SERVER['HTTP_HOST']?>#dopolnitelnye" class="header_menu_link">Еще</a>
					</li>
					<!--<li class="header_menu__item col-12 col-lg col-sm-12 col-lg-auto">-->
					<!--<a href="javascript:void(0);" class="header_menu_link"></a>-->
					<!--</li>-->
				</ul>
			</div>
		</div>
	</div>
</div>