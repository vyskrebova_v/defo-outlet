<?php include 'header.php';?>
		<div class="advantages_container container container">
			<div class="advantages row justify-content-between">
				<div class="advantages__item col col-lg-auto justify-content-center">
					<div class="advantages__icon"></div>
					<div class="advantages__text">Максимальные скидки</div>
				</div>
				<div class="advantages__item col col-lg-auto justify-content-center">
					<div class="advantages__icon"><span class="advantages_icon__text up" data-after="2">1000 м</span></div>
					<div class="advantages__text">Большая выставка</div>
				</div>
				<div class="advantages__item col col-lg-auto justify-content-center">
					<div class="advantages__icon"></div>
					<div class="advantages__text">Европейский дизайн</div>
				</div>
				<div class="advantages__item col col-lg-auto justify-content-center">
					<div class="advantages__icon"><span class="advantages_icon__text">1-2 дня</span></div>
					<div class="advantages__text">Быстрая доставка</div>
				</div>
				<div class="advantages__item col col-lg-auto justify-content-center">
					<div class="advantages__icon"></div>
					<div class="advantages__text">Тысячи моделей</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="banners__container container">
				<div class="banners row">
					<div id="carouselBanners" class="carousel slide banners__col col-sm-6 col-12" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselBanners" data-slide-to="0" class="active"></li>
							<li data-target="#carouselBanners" data-slide-to="1"></li>
						</ol>
						<div class="carousel-inner">
							<div class="carousel-item active">
								<a href="#stoly">
									<img src="images/banner_1.png" alt="Дэфо Итальянский стиль" class="banners__img d-block w-100">
								</a>
							</div>
							<div class="carousel-item">
								<a href="#gostinnye">
									<img src="images/banners_slide_2.png" alt="Дэфо Итальянский стиль" class="banners__img d-block w-100">
								</a>
							</div>
						</div>
					</div>
					<div class="banners__col col-sm col-6 row">
						<a href="#stoly" class="banner__inner_col col-lg-12">
							<img src="images/banner_2.png" alt="Дэфо Обеденные столы" class="banners__img">
						</a>
						<a href="#stulya-dlya-doma" class="banner__inner_col col-lg-12">
							<img src="images/banner_3.png" alt="Дэфо Стулья для дома" class="banners__img">
						</a>
					</div>
					<a href="#komputernye-kresla" class="banners__col col-sm col-6">
						<img src="images/banner_4.png" alt="Дэфо Компьютерные кресла" class="banners__img">
					</a>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="container">
				<div class="subtitle row">
					<h2>Лучшие предложения</h2>
				</div>
				<div class="product_top row">
					<div class="product__item col-lg col-md col-6">
						<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
							<img src="images/products/kamni.jpg" alt="Камни" class="product_img rounded mx-auto d-block">
						</a>
						<div class="product_colors">
							<div class="product_colors__item" style="background: #E792BA;"></div>
						</div>
						<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
							<span class="product_title">Камни</span>
						</a>
						<div class="product__prices">
							<div class="product_prices__old">
								<span class="product_price">8 645 ₽</span>
							</div>
							<div class="product_prices__new">
								<span class="product_price">4 800 ₽</span>
								<span class="product_discount">-44%</span>
							</div>
						</div>
					</div>
					<div class="product__item col-lg col-md col-6">
						<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
							<img src="images/products/joy.jpg" alt="Стул для дома Joy / Джой" class="product_img rounded mx-auto d-block">
						</a>
						<div class="product_colors">
							<div class="product_colors__item" style="background: #908885;"></div>
						</div>
						<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
							<span class="product_title">Стул для дома Joy / Джой</span>
						</a>
						<div class="product__prices">
							<div class="product_prices__old">
								<span class="product_price">15 831 ₽</span>
							</div>
							<div class="product_prices__new">
								<span class="product_price">7 900 ₽</span>
								<span class="product_discount">-50%</span>
							</div>
						</div>
					</div>
					<div class="product__item col-lg col-md col-6">
						<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
							<img src="images/products/bruno.jpg" alt="Компьютерное кресло Bruno / Бруно" class="product_img rounded mx-auto d-block">
						</a>
						<div class="product_colors">
							<div class="product_colors__item" style="background: #32769D;"></div>
							<div class="product_colors__item" style="background: #569C61;"></div>
						</div>
						<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
							<span class="product_title">Компьютерное кресло Bruno / Бруно</span>
						</a>
						<div class="product__prices">
							<div class="product_prices__old">
								<span class="product_price">16 599 ₽</span>
							</div>
							<div class="product_prices__new">
								<span class="product_price">6 900 ₽</span>
								<span class="product_discount">-58%</span>
							</div>
						</div>
					</div>
					<div class="product__item col-lg col-md col-6">
						<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
							<img src="images/products/team.jpg" alt="Стол Team 1 A-4146 / Тим 1" class="product_img rounded mx-auto d-block">
						</a>
						<div class="product_colors">
							<div class="product_colors__item" style="background: #A9A198;"></div>
							<div class="product_colors__item" style="background: #FFFFFF;"></div>
						</div>
						<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
							<span class="product_title">Стол Team 1 A-4146 / Тим 1</span>
						</a>
						<div class="product__prices">
							<div class="product_prices__old">
								<span class="product_price">55 678 ₽</span>
							</div>
							<div class="product_prices__new">
								<span class="product_price">28 300 ₽</span>
								<span class="product_discount">-49%</span>
							</div>
						</div>
					</div>
				</div>
				<div class="news row text-center">
					<a href="/info#how" class="news_col col-lg col-md col-12">
						<div class="news__wrap_img">
							<img src="images/news-1.png" alt="Discount" class="news__img">
						</div>
						<div class="news__title d-flex justify-content-center align-items-center">
							<span class="news_title__span">Скидки более 50%</span>
						</div>
						<div class="hover-bg"></div>
					</a>
					<a href="/info#advantages" class="news_col col-lg col-md col-12">
						<div class="news__wrap_img">
							<img src="images/news-2.png" alt="Discount" class="news__img">
						</div>
						<div class="news__title d-flex justify-content-center align-items-center">
							<span class="news_title__span">Наши преимущества</span>
						</div>
						<div class="hover-bg"></div>
					</a>
					<a href="/info#about" class="news_col col-lg col-md col-12">
						<div class="news__wrap_img">
							<img src="images/news-3.png" alt="Discount" class="news__img">
						</div>
						<div class="news__title d-flex justify-content-center align-items-center">
							<span class="news_title__span">О компании</span>
						</div>
						<div class="hover-bg"></div>
					</a>
				</div>
				<div class="subtitle row">
					<h2>Все товары</h2>
				</div>
				<div class="product_all row">
					<div class="product_category col-12 row" id="stoly">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/leaden.jpg" alt="Стол Leeden NS 9289 / Лиден" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #006E71;"></div>
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Leeden NS 9289 / Лиден</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">18 897 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">14 500 ₽</span>
									<span class="product_discount">-23%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/mario.jpg" alt="Стол Mario / Марио" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Mario / Марио</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">17 253 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">13 900 ₽</span>
									<span class="product_discount">-19%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/leaden-ns9275.jpg" alt="Стол Leeden NS 9275 / Лиден" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #E0E0E0;"></div>
								<div class="product_colors__item" style="background: #B19379;"></div>
								<div class="product_colors__item" style="background: #635957;"></div>
								<div class="product_colors__item" style="background: #46393A;"></div>
								<div class="product_colors__item" style="background: #695E5D;"></div>
								<div class="product_colors__item" style="background: #DFDFDF;"></div>
								<div class="product_colors__item" style="background: #3D3333;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Leeden NS 9275 / Лиден</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">13 780 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">9 299 ₽</span>
									<span class="product_discount">-33%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/team.jpg" alt="Стол Team 1 A-4146 / Тим 1" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #A9A198;"></div>
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Team 1 A-4146 / Тим 1</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">55 678 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">28 300 ₽</span>
									<span class="product_discount">-49%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/leeden-s9228.jpg" alt="Стол Leeden S 9228 / Лиден" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #C5C4C9;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Leeden S 9228 / Лиден</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">37 473 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">20 999 ₽</span>
									<span class="product_discount">-44%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/team-1-a-4130.jpg" alt="Стол Team 1 A-4130 / Тим 1" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Team 1 A-4130 / Тим 1</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">40 416 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">27 999 ₽</span>
									<span class="product_discount">-31%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/team-1-a-4118.jpg" alt="malta" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #907F77;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Team 1 A-4118 / Тим 1</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">53 974 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">40 000 ₽</span>
									<span class="product_discount">-26%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/team-short-a-4146.jpg" alt="Стол Team Short A-4146 / Тим Шорт А-4146" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стол Team Short A-4146 / Тим Шорт А-4146</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">50 736 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">37 400 ₽</span>
									<span class="product_discount">-26%</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="stulya-dlya-doma">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/ed.jpg" alt="Стул для дома Ed" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #787878;"></div>
								<div class="product_colors__item" style="background: #BE4D44;"></div>
								<div class="product_colors__item" style="background: #BCBCBC;"></div>
								<div class="product_colors__item" style="background: #A28269;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Ed</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">5 073 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 295 ₽</span>
									<span class="product_discount">-35%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/lee.jpg" alt="Стул для дома Lee / Ли" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #DE3126;"></div>
								<div class="product_colors__item" style="background: #909090;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Lee / Ли</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">5 304 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 444 ₽</span>
									<span class="product_discount">-35%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/joy.jpg" alt="Стул для дома Joy / Джой" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #908885;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Joy / Джой</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">15 831 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">7 900 ₽</span>
									<span class="product_discount">-50%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/lester.jpg" alt="Стул для дома Lester / Лестер" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #D84D26;"></div>
								<div class="product_colors__item" style="background: #668A95;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Lester / Лестер</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">8 480 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">5 600 ₽</span>
									<span class="product_discount">-34%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/nexus.jpg" alt="Стул для дома Nexus / Нексус" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #4F0C7B;"></div>
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
								<div class="product_colors__item" style="background: #000000;"></div>
								<div class="product_colors__item" style="background: #B9010D;"></div>
								<div class="product_colors__item" style="background: #3F2524;"></div>
								<div class="product_colors__item" style="background: #7D7F7E;"></div>
								<div class="product_colors__item" style="background: #1F4023;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Nexus / Нексус</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">5 810 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 900 ₽</span>
									<span class="product_discount">-33%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/simple-chrome.jpg" alt="Стул для дома Simple Chrome / Симпл Хром" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #A5E641;"></div>
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
								<div class="product_colors__item" style="background: #DF7C17;"></div>
								<div class="product_colors__item" style="background: #E28016;"></div>
								<div class="product_colors__item" style="background: #000000;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Simple Chrome / Симпл Хром</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">4 390 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">2 999 ₽</span>
									<span class="product_discount">-32%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/vera.jpg" alt="Стул для дома Vera / Вера" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #BFAA91;"></div>
								<div class="product_colors__item" style="background: #3C2725;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Vera / Вера</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">7 317 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">4 500 ₽</span>
									<span class="product_discount">-38%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/fin.jpg" alt="Стул для дома Fin 02 / Фин 02" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #BBB4AE;"></div>
								<div class="product_colors__item" style="background: #3B302A;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Стул для дома Fin 02 / Фин 02</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__new">
									<span class="product_price">7 986 ₽</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="komputernye-kresla">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/bruno.jpg" alt="Компьютерное кресло Bruno / Бруно" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #32769D;"></div>
								<div class="product_colors__item" style="background: #569C61;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Bruno / Бруно</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">16 599 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">6 900 ₽</span>
									<span class="product_discount">-58%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/click.jpg" alt="Компьютерное кресло Kit / Кит" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #8A8A8A;"></div>
								<div class="product_colors__item" style="background: #000000;"></div>
								<div class="product_colors__item" style="background: #F18634;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Click / Клик</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">5 399 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 900 ₽</span>
									<span class="product_discount">-28%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/kit.jpg" alt="Компьютерное кресло Kit / Кит" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #E48764;"></div>
								<div class="product_colors__item" style="background: #CFDD64;"></div>
								<div class="product_colors__item" style="background: #757575;"></div>
								<div class="product_colors__item" style="background: #3B51BC;"></div>
								<div class="product_colors__item" style="background: #000000;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Kit / Кит</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">4 399 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">2 330 ₽</span>
									<span class="product_discount">-47%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/profy.jpg" alt="Компьютерные кресла Proffy / Профи" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #CB8D58;"></div>
								<div class="product_colors__item" style="background: #494949;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Proffy / Профи</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">8 299 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 860 ₽</span>
									<span class="product_discount">-53%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/zumer.jpg" alt="Компьютерные кресла Zumer / Зумер" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #000000;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Zumer / Зумер</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">7 999 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 830 ₽</span>
									<span class="product_discount">-53%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/lark.jpg" alt="Компьютерное кресло Lark / Ларк" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #75CE57;"></div>
								<div class="product_colors__item" style="background: #E86F40;"></div>
								<div class="product_colors__item" style="background: #000000;"></div>
								<div class="product_colors__item" style="background: #F53348;"></div>
								<div class="product_colors__item" style="background: #5D74B9;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Lark / Ларк</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">7 099 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">3 999 ₽</span>
									<span class="product_discount">-44%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/runa.jpg" alt="Компьютерное кресло Runa / Руна" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #C0B7B0;"></div>
								<div class="product_colors__item" style="background: #644B3F;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Runa / Руна</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">18 799 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">12 900 ₽</span>
									<span class="product_discount">-31%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/renome.jpg" alt="Компьютерное кресло Renome / Реноме" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #E15958;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Компьютерное кресло Renome / Реноме</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">14 999 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">8 900 ₽</span>
									<span class="product_discount">-41%</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="gostinnye">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/indigo.jpg" alt="Гостинная Индиго" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #32769D;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Гостинная Индиго</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">99 503 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">47 770 ₽</span>
									<span class="product_discount">-52%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/solo.jpg" alt="Комплект Гостиной Соло №1" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #E5E4DF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Комплект Гостиной Соло №1</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">63 210 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">49 900 ₽</span>
									<span class="product_discount">-21%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/fendi.jpg" alt="Гостиная Фенди Комплект №1" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #F9F2EC;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Гостиная Фенди Комплект №1</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">54 680 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">43 200 ₽</span>
									<span class="product_discount">-21%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/rosso.jpg" alt="Гостиная Rosso" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #5B5A56;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Гостиная Rosso</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">50 860 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">20 200 ₽</span>
									<span class="product_discount">-21%</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="detskie">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/mia.jpg" alt="Детская Mia" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #C8BEC1;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Детская Mia</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">43 933 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">13 180 ₽</span>
									<span class="product_discount">-70%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/dushes.jpg" alt="Детская Dushes" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #B8276C;"></div>
								<div class="product_colors__item" style="background: #A3B174;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Детская Dushes</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">57 628 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">50 500 ₽</span>
									<span class="product_discount">-12%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/blake.jpg" alt="Детская Blake" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #AE8E68;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Детская Blake</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">33 000 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">39 068 ₽</span>
									<span class="product_discount">-16%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/brown.jpg" alt="Детская Brown" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #AE8E68;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Детская Brown</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">23 750 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">20 000 ₽</span>
									<span class="product_discount">-16%</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="spalni">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/marika.jpg" alt="Спальня Marika" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #F5E9D3;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Спальня Marika</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">15 250 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">12 200 ₽</span>
									<span class="product_discount">-20%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/ademi.jpg" alt="Спальня Ademi" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Спальня Ademi</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">36 566 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">30 000 ₽</span>
									<span class="product_discount">-18%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/palermo.jpg" alt="Спальня Palermo" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #A76F49;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Спальня Palermo</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">25 329 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">19 700 ₽</span>
									<span class="product_discount">-22%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/leona.jpg" alt="Спальня Leona" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Спальня Leona</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">28 758 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">23 500 ₽</span>
									<span class="product_discount">-18%</span>
								</div>
							</div>
						</div>
					</div>
					<div class="product_category col-12 row" id="dopolnitelnye">
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/modern.jpg" alt="Modern C10045 / Модерн С10045" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #FFFFFF;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Modern C10045 / Модерн С10045</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">34 818 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">21 000 ₽</span>
									<span class="product_discount">-40%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/shah.jpg" alt="Шах и мат" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #BBBCC0;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Шах и мат</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">12 100 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">6 700 ₽</span>
									<span class="product_discount">-45%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/kamni.jpg" alt="Камни" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #E792BA;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Камни</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">8 645 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">4 800 ₽</span>
									<span class="product_discount">-44%</span>
								</div>
							</div>
						</div>
						<div class="product__item col-lg-3 col-md-3 col-6">
							<a href="javascript:void(0);" class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order">
								<img src="images/products/enter-glass.jpg" alt="Enter Glass 5 / Энтер Гласс" class="product_img rounded mx-auto d-block">
							</a>
							<div class="product_colors">
								<div class="product_colors__item" style="background: #000000;"></div>
							</div>
							<a href="javascript:void(0);" class="product_title__a" data-toggle="modal" data-target="#make_order">
								<span class="product_title">Enter Glass 5 / Энтер Гласс</span>
							</a>
							<div class="product__prices">
								<div class="product_prices__old">
									<span class="product_price">7 440 ₽</span>
								</div>
								<div class="product_prices__new">
									<span class="product_price">4 499 ₽</span>
									<span class="product_discount">-40%</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="container yet"><a href="javascript:void(0);">Показать еще</a></div>
		</div>
<?php include 'footer.php';?>