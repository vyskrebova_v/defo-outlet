<div class="container-fluid footer">
	<div class="footer__content container">
		<div class="footer__row row">
			<div class="footer__col col-12 col-sm row">
				<span class="footer_contacts__place col-12">Санкт-Петербург, МЦ Аквилон</span>
				<span class="footer_contacts__phone col-12">
							<a href="tel:8 (812) 596-34-76" class="contacts_phone__link">8 (812) 596-34-76</a>
						</span>
				<span class="footer_contacts__address col-12">Новолитовская ул., 15 Д</span>
			</div>
			<div class="footer__col col-12 col-sm row">
				<span class="footer_contacts__place col-12">Москва, ТЦ Горбушкин двор</span>
				<span class="footer_contacts__phone col-12">
							<a href="tel:8 (812) 596-34-76" class="contacts_phone__link">8 (812) 596-34-76</a>
						</span>
				<span class="footer_contacts__address col-12">Багратионовский пр., 7</span>
			</div>
			<div class="footer__col col-12 col-sm-auto">
				<ul class="footer__link_page row justify-content-around">
					<li class="footer_link_page__item col-6 col-sm-12">
						<a href="javascript:void(0);" class="footer_link">Доставка и оплата</a>
					</li>
					<li class="footer_link_page__item col-6 col-sm-12">
						<a href="javascript:void(0);" class="footer_link">Гарантия</a>
					</li>
				</ul>
			</div>
			<div class="footer__col col-12 col-sm-auto text-center">
				<a href="https://www.defo.ru" class="footer__link">defo.ru</a>
			</div>
		</div>
	</div>
</div>
<div id="scrollup" class="scrollup" style="display: block;">
	<div class="scrollbg"></div>
	<div id="scrollup_arrow" class="scrollup_arrow" style="display: block;">
		<?= file_get_contents ( $_SERVER['DOCUMENT_ROOT'].'/images/icons/arrow_top.svg' );?>
	</div>
</div>

<div class="modal fade" id="make_order" tabindex="-1" role="dialog" aria-labelledby="make_order__label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header align-self-center">
				<h5 class="modal-title" id="make_order__label">New message</h5>
			</div>
			<div class="modal-body row">
				<div class="col-12 col-md-6 align-self-center">
					<div class="product-afisha justify-self-center">
						<div class="product__image d-flex align-items-center" data-toggle="modal" data-target="#make_order"></div>
						<div class="product_colors"></div>
						<div class="product__prices"></div>
					</div>
				</div>
				<form class="col-12 col-md-6 align-self-start text-center">
					<div class="form_text text-center">
						<p class="form_text__p">Оставьте свои контактные данные и мы свяжемся с вами</p>
					</div>
					<div class="form-group text-left">
						<label for="personal_phone" class="col-form-label">Телефон</label>
						<input type="text" name="phone" class="form-control" id="personal_phone" placeholder="+7(___) ___-__-__" required>
						<label for="personal_email" class="col-form-label">Email</label>
						<input type="email" name="email" class="form-control" id="personal_email" placeholder="Email">
						<label for="city_choose" class="col-form-label">Город</label>
						<select name="city" class="form-control" id="city_choose" required>
							<option value="Ваш город" selected disabled>Ваш город *</option>
							<option value="Москва">Москва</option>
							<option value="Санкт-Петербург">Санкт-Петербург</option>
						</select>
					</div>
					<input type="hidden" name="product_name">
					<input type="hidden" name="product_img">

					<input type="submit" class="btn btn-primary" value="Купить" onclick="validate(this.form);">
				</form>
			</div>
			<div class="modal-footer">
			</div>
		</div>
		<?/*
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		*/?>
	</div>
</div>

<!-- Optional JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="//<?= $_SERVER['HTTP_HOST']?>/js/jquery.maskedinput.min.js"></script>
<script src="//<?= $_SERVER['HTTP_HOST']?>/js/script.js" ></script>

<script>

	$('#make_order').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.data('whatever') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var product_item = button.parent();
		var modal = $(this);

		//name product
		modal.find('.modal-title').text(product_item.find('.product_title').html());
		modal.find('input[name=product_name]').val(product_item.find('.product_title').html());

		//info product
		modal.find('.product__image').html(product_item.find('.product__image').html());
		modal.find('.product_colors').html(product_item.find('.product_colors').html());
		modal.find('.product__prices').html(product_item.find('.product__prices').html());
	});
</script>
</body>
</html>