$(document).ready(function () {
	menu();
	$(window).resize(function () {
		menu();
	});

    $("#personal_phone").mask("+7(999) 999-99-99");

    $('#make_order form').sendForm({
        mailUrl: "./send.php"
    });

    $('#scrollup').click(function () {
        $('body,html').animate({scrollTop: 0}, 1500);
    })
});

function menu() {
	if($(window).width() <= 991){
		header = $('.header');
		body = $('body');
		$('.header__logo').on('click', function(){
			if(header.hasClass('open_menu')) {
				header.removeClass('open_menu');
				body.css('overflow', 'auto');
			} else {
				header.addClass('open_menu');
				body.css('overflow', 'hidden');
			}
		})
	}
}
// Валидация формы
function validate(form){
    $this = $(form);
    console.log($this);
    $(form).find('input, textarea').each(function(){
        if($(this).prop('required') && $(this).val() == ''){
            console.log($(this));
            $(this).addClass('validation-error');
            errorMessage = 'Заполните пожалуйста поле '+ $(this).data('title');
            $this.prepend('<div class="form__error">' + errorMessage + '</div>');
            setTimeout(function() {
                $this.find('.form__error').remove();
            }, 5000);
        } else {
            $(this).removeClass('validation-error');
        }
    });
}

(function($) {
    jQuery.fn.sendForm = function(options) {
        options = $.extend({
            successTitle: "Спасибо, что выбрали нас!",
            successText: "Мы свяжемся с Вами в ближайшее время.",
            errorTitle: "Сообщение не отправлено!",
            errorSubmit: "Ошибка отправки формы!",
            errorNocaptcha: "Вы не заполнили каптчу",
            errorCaptcha: "Вы не прошли проверку каптчи",
            mailUrl: "./send.php",
            autoClose: false,
            autoCloseDelay: 5000
        }, options);

        var make = function() {
            var $this = $(this);
            $(this).submit(function() {
                function errorRes(errorMessage) {
                    $this.prepend('<div class="form__error">' + errorMessage + '</div>');
                    setTimeout(function() {
                        $this.find('.form__error').remove();
                    }, 5000);
                }
                var data = $(this).serialize();
                $.ajax({
                    url: options.mailUrl,
                    type: "POST",
                    data: data,
                    beforeSend: function() {
                    },
                    success: function(res) {
                        if (res == 1) {
                            $this[0].reset();
                            // grecaptcha.reset();
                            $this.find('.form__hide-success').slideUp().delay(5000).slideDown();
                            $this.find('.form__hide-success').after('<div class="form__sys-message"></div>');
                            $this.find('.form__sys-message').html('<div class="form__success-title">' + options.successTitle + '</div><p class = "form__success-text" >' + options.successText + '</p>');
                            setTimeout(function() {
                                $this.find('.form__sys-message').fadeOut().delay(3000).remove();
                                if (options.autoClose) {
                                    $.magnificPopup.close();
                                }
                            }, options.autoCloseDelay);
                        } else if (res == 2) {
                            errorRes(options.errorNocaptcha);
                        } else if (res == 3) {
                            errorRes(options.errorCaptcha);
                        } else {
                            errorRes(options.errorSubmit);
                        }
                    },
                    error: function() {
                        errorRes(options.errorSubmit);
                    }
                });
                return false;
            });
        }
        return this.each(make);
    };
})(jQuery);